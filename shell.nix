let
  oxalica_overlay = import (builtins.fetchTarball
    "https://github.com/oxalica/rust-overlay/archive/master.tar.gz");
  nixpkgs = import <nixpkgs> { overlays = [ oxalica_overlay ]; };

in with nixpkgs;

stdenv.mkDerivation {
  name = "rust-env";
  buildInputs = [
    (rust-bin.stable."1.70.0".default.override { extensions = [ "rust-src" ]; })
    

    # for auditing
    cargo-deny
    cargo-audit
    cargo-auditable

    # spell checker
    typos
  ];

  # Set Environment Variables
  RUST_BACKTRACE = 1;

}


